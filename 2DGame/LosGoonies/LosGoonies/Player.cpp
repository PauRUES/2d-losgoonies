#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Player.h"
#include "Game.h"
#include <mmsystem.h>


#define JUMP_ANGLE_STEP 4
#define JUMP_HEIGHT 66
#define FALL_STEP 4

#define SCREEN_X 32
#define SCREEN_Y 20


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT, MOVE_DOWN, JUMP_LEFT, JUMP_RIGHT, ATTACK_LEFT, ATTACK_RIGHT
};


void Player::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	int lp = 81;
	int exp = 81;
	texProgram = shaderProgram; 
	bJumping = false;
	climbing = false; 
	changing_screen = false; 
	spritesheet.loadFromFile("images/player.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32,32), glm::vec2(1/3.f, 1/7.f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(9);
	
	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 4/7.f));
		
	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.0f, 1/7.f));
		
	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 4/7.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(1/3.f, 4/7.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(2/3.f, 4/7.f));
		
	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 1/7.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1/3.f, 1/7.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(2/3.f, 1/7.f));

	sprite->setAnimationSpeed(MOVE_DOWN, 8);
	sprite->addKeyframe(MOVE_DOWN, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_DOWN, glm::vec2(1 / 3.f, 0.f));

	sprite->setAnimationSpeed(ATTACK_LEFT, 8);
	sprite->addKeyframe(ATTACK_LEFT, glm::vec2(0.f, 6/7.f));

	sprite->setAnimationSpeed(ATTACK_RIGHT, 8);
	sprite->addKeyframe(ATTACK_RIGHT, glm::vec2(0.f, 3/7.f));

	sprite->setAnimationSpeed(JUMP_LEFT, 8);
	sprite->addKeyframe(JUMP_LEFT, glm::vec2(0.f, 5/7.f));

	sprite->setAnimationSpeed(JUMP_RIGHT, 8);
	sprite->addKeyframe(JUMP_RIGHT, glm::vec2(0.f, 2/7.f));
		
	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	
}

void Player::update(int deltaTime)
{
	sprite->update(deltaTime);
	int endScreen = map->endScreen(posPlayer);

	if (endScreen != -1 && !changing_screen) {
	
		glm::ivec2 newPosPlayer = map->changeScreen(endScreen, glm::vec2(SCREEN_X, SCREEN_Y),posPlayer, texProgram);
		posPlayer.x = newPosPlayer.x;
		posPlayer.y = newPosPlayer.y; 
	}
	
	if (Game::instance().getSpecialKey(GLUT_KEY_LEFT))
	{
		if (!climbing) {
			if (sprite->animation() != MOVE_LEFT)
				sprite->changeAnimation(MOVE_LEFT);
			posPlayer.x -= 2;
			if (map->collisionMoveLeft(posPlayer, glm::ivec2(16, 16)))
			{
				posPlayer.x += 2;
				sprite->changeAnimation(STAND_LEFT);
			}
		}
	}
	else if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT))
	{
		if (!climbing) {
			if (sprite->animation() != MOVE_RIGHT)
				sprite->changeAnimation(MOVE_RIGHT);
			posPlayer.x += 2;
			if (map->collisionMoveRight(posPlayer, glm::ivec2(16, 16)))
			{
				posPlayer.x -= 2;
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
	}
	else
	{
		if (sprite->animation() == MOVE_LEFT)
			sprite->changeAnimation(STAND_LEFT);
		else if (sprite->animation() == MOVE_RIGHT)
			sprite->changeAnimation(STAND_RIGHT);
	}

	if (bJumping)
	{
		jumpAngle += JUMP_ANGLE_STEP;
		if (jumpAngle == 180)
		{
			bJumping = false;
			posPlayer.y = startY;
			if (sprite->animation() == JUMP_LEFT)
				sprite->changeAnimation(STAND_LEFT);
			else
				sprite->changeAnimation(STAND_RIGHT);
		}
		else
		{
			if (posYMax == 66)
				posPlayer.y = int(startY - posYMax * sin(3.14159f * jumpAngle / 180));
			else 
				posPlayer.y = int(startY - (startY-posYMax) * sin(3.14159f * jumpAngle / 180));
			if (jumpAngle > 90) {
				bJumping = !map->collisionMoveDown(posPlayer, glm::ivec2(16, 16), &posPlayer.y);
				if (!bJumping) {
					if (sprite->animation() == JUMP_LEFT)
						sprite->changeAnimation(STAND_LEFT);
					else
						sprite->changeAnimation(STAND_RIGHT);
				}
			} 
			else {
				if (map->collisionMoveUp(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && jumpAngle < 92) {
					posYMax = posPlayer.y;
					jumpAngle = 92;
					posPlayer.y = int(startY - (startY-posYMax) * sin(3.14159f * jumpAngle / 180));
				}
			}
		}
	}
	else if(climbing){
		if (map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && Game::instance().getSpecialKey(GLUT_KEY_UP)) {
			posPlayer.y -= 2;
		}

		else if (!map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
			climbing = false; 
			sprite->changeAnimation(STAND_LEFT);
		}

		if (map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && Game::instance().getSpecialKey(GLUT_KEY_DOWN)) {
			posPlayer.y += 2;
		}
		else if (!map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
			climbing = false;
			sprite->changeAnimation(STAND_LEFT);
		}
	}
	else
	{
		posPlayer.y += FALL_STEP;
		if (map->collisionMoveDown(posPlayer, glm::ivec2(16, 16), &posPlayer.y))
		{
			if (Game::instance().getSpecialKey(GLUT_KEY_UP))
			{
				if (map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					climbing = true; 
					posPlayer.y -= 2;
					sprite->changeAnimation(MOVE_DOWN);
					
				}
				else if (map->puerta(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					/*se cambia de mapa*/
				}
				/*else if (map->collisionMoveUp(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					jumpAngle = 180;
					posPlayer.y = int(startY -  * sin(3.14159f * jumpAngle / 180.f));
				}*/
				else {
					PlaySound(TEXT("sounds/jump.wav"), NULL, SND_FILENAME | SND_ASYNC);
					bJumping = true;
					jumpAngle = 0;
					jumpAngleMax = 180.f;
					posYMax = 66;
					startY = posPlayer.y;
					if (sprite->animation() == MOVE_LEFT || sprite->animation() == STAND_LEFT)
						sprite->changeAnimation(JUMP_LEFT);
					else if (sprite->animation() == MOVE_RIGHT || sprite->animation() == STAND_RIGHT)
						sprite->changeAnimation(JUMP_RIGHT);

				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_DOWN))
			{
				if (map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					climbing = true;
					sprite->changeAnimation(MOVE_DOWN);
					posPlayer.y += 2;
				}
			}
		}
	}

	if (Game::instance().getKey(97)) {
		PlaySound(TEXT("sounds/hit.wav"), NULL, SND_FILENAME | SND_ASYNC);
		if (sprite->animation() == MOVE_LEFT || sprite->animation() == STAND_LEFT) {
			sprite->changeAnimation(ATTACK_LEFT);
			//sprite->changeAnimation(STAND_LEFT);
		}
		else if (sprite->animation() == MOVE_RIGHT || sprite->animation() == STAND_RIGHT) {
			sprite->changeAnimation(ATTACK_RIGHT);
			//sprite->changeAnimation(STAND_RIGHT);
		}
	}


	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
}

void Player::render()
{
	sprite->render();
}

void Player::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Player::setBars(TileMap *tileMap)
{
	bars = tileMap;
}

void Player::setPosition(const glm::vec2 &pos)
{
	posPlayer = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
}

void Player::lose_lp(int times) { //times indica el numero de veces que se resta 5 a la vida (algunas trampas hacen mas da�o, otras menos...)
	lp -= times;
	bars->updateBar(lp, 0);
	/*if (lp <= 0)
		se llama a game over*/
}

void Player::gain_lp(int times) { //times indica las veces que gana 5 de vida (dependiendo de la exp al cambiar de pantalla por ejemplo)
	lp += times;
	bars->updateBar(lp, 0);
	if (lp > 81)
		lp = 81;
}

void Player::lose_exp(int times) { //dependiendo del enemigo, trampa...sss
	exp -= times;
	bars->updateBar(lp, 1);
	if (exp < 0)
		exp = 0;
}

void Player::gain_exp(int times) { //dependiendo del enemigo da mas o menos exp, coger las bolsas de dinero...
	exp += times;
	bars->updateBar(lp, 1);
	if (exp > 81)
		exp = 81;
}

int Player::getLp() {
	return lp;
}

bool Player::doorcollition() {
	return map->puerta(posPlayer, glm::ivec2(16, 16), &posPlayer.y);
}




