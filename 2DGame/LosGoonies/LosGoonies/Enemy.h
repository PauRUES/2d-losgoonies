#ifndef _ENEMY_INCLUDE
#define _ENEMY_INCLUDE

#include "TileMap.h"
#include "Sprite.h"

class Enemy {

public:
	Enemy();
	void init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, string type);
	void update(int deltaTime);
	void render();


	int attack;
	string typeEnemy;


private:

	Texture spritesheet;
	ShaderProgram texProgram;
	Sprite* sprite;
	glm::ivec2 tileMapDispl, posEnemy;
	int width_attack, startX, attackAngle;
};


#endif // _ENEMY_INCLUDE