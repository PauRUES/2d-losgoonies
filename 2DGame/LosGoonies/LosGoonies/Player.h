#ifndef _PLAYER_INCLUDE
#define _PLAYER_INCLUDE


#include "Sprite.h"
#include "TileMap.h"


// Player is basically a Sprite that represents the player. As such it has
// all properties it needs to track its movement, jumping, and collisions.


class Player
{

public:
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime);
	void render();
	
	void setTileMap(TileMap *tileMap);
	void setPosition(const glm::vec2 &pos);

	void lose_lp(int times);
	void gain_lp(int times);
	void lose_exp(int times);
	void gain_exp(int times);

	int getLp();
	bool doorcollition();

	void setBars(TileMap *tileMap);
	
private:
	ShaderProgram texProgram; 
	bool bJumping;
	bool climbing; 
	bool changing_screen; 
	glm::ivec2 tileMapDispl, posPlayer;
	int jumpAngle, startY, posYMax;
	float jumpAngleMax;
	Texture spritesheet;
	Sprite *sprite;
	TileMap *map;
	TileMap *bars;
	int lp;
	int exp;

};


#endif // _PLAYER_INCLUDE


