#include <iostream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "Scene.h"
#include "Game.h"
#include <windows.h>
#include <GL/freeglut_std.h>
#include <mmsystem.h>


#define SCREEN_X 32
#define SCREEN_Y 28

#define INIT_PLAYER_X_TILES 2
#define INIT_PLAYER_Y_TILES 12


Scene::Scene()
{
	//map = NULL;
	player = NULL;
}

Scene::~Scene()
{
	/*if(map != NULL)
		delete map;*/
	for (auto kv : layout) {
		if (kv.second)
			delete kv.second;
	}
	if(player != NULL)
		delete player;
}


void Scene::init()
{
	initShaders();
	TileMap* map;
	//map = TileMap::createTileMap("levels/menu.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 2);
	/*map = TileMap::createTileMap("levels/menu.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	map = TileMap::createTileMap("levels/press_start.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;*/
	player = new Player();
	player->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	map = TileMap::createTileMap("levels/punctuation.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	map = TileMap::createTileMap("levels/bars.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;
	player->setBars(map);
	map = TileMap::createTileMap("levels/escena1.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 2, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
	projection = glm::ortho(0.f, float(SCREEN_WIDTH - 1), float(SCREEN_HEIGHT - 1), 0.f);
	currentTime = 0.0f;
	menu = true;
	instructions = false;
	gameover = false;
	bplayer = true;
	scene = 0;
	//change_scene1();

}

void Scene::update(int deltaTime)
{
	currentTime += deltaTime;
	if (bplayer)
		player->update(deltaTime);

	/*if (Game::instance().getKey(13) && menu) {
		menu = false;
		instructions = true;
		Sleep(1000);
		change_instructions();
	}
	else if (Game::instance().getKey(13) && instructions) {
		instructions = false;
		scene = 1;
		Sleep(1000);
		change_scene1();
	}
	else if (scene != 0 && Game::instance().getSpecialKey(GLUT_KEY_UP) && player->doorcollition()) {
		scene += 1;
		if (scene == 2) {
			Sleep(1000);
			change_scene2();
		}
		if (scene == 3) {
			Sleep(1000);
			change_scene3();
		}
		if (scene == 4) {
			Sleep(1000);
			change_scene4();
		}
		else if (scene == 5) {
			Sleep(1000);
			change_scene5();
		}
		else {
			Sleep(1000);
			change_credits();
			if (Game::instance().getKey(13))
				init();
		}
	}
	else if (scene != 0 && player->getLp() == 0) {
		game_over();
		Sleep(3000);
		init();
	}*/
}

void Scene::render()
{
	glm::mat4 modelview;

	texProgram.use();
	texProgram.setUniformMatrix4f("projection", projection);
	texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
	modelview = glm::mat4(1.0f);

	texProgram.setUniformMatrix4f("modelview", modelview);
	texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);
	for (int i = 0; i < layout.size(); ++i) {
		layout[i]->render();
	}
	if (bplayer)
		player->render();
}

void Scene::initShaders()
{
	Shader vShader, fShader;

	vShader.initFromFile(VERTEX_SHADER, "shaders/texture.vert");
	if(!vShader.isCompiled())
	{
		cout << "Vertex Shader Error" << endl;
		cout << "" << vShader.log() << endl << endl;
	}
	fShader.initFromFile(FRAGMENT_SHADER, "shaders/texture.frag");
	if(!fShader.isCompiled())
	{
		cout << "Fragment Shader Error" << endl;
		cout << "" << fShader.log() << endl << endl;
	}
	texProgram.init();
	texProgram.addShader(vShader);
	texProgram.addShader(fShader);
	texProgram.link();
	if(!texProgram.isLinked())
	{
		cout << "Shader Linking Error" << endl;
		cout << "" << texProgram.log() << endl << endl;
	}
	texProgram.bindFragmentOutput("outColor");
	vShader.free();
	fShader.free();
}

void Scene::initial_screen() {
	TileMap* map;
	map = TileMap::createTileMap("levels/press_start.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;
	if(menu) {
		change_instructions();
	}
		//Sleep(1000);
		delete layout[0];
		delete layout[1];
		/*delete map;
		Sleep(1000);
	}*/
	
}

void Scene::change_scene1() {
	TileMap* map;
	player = new Player();
	player->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	map = TileMap::createTileMap("levels/punctuation.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	map = TileMap::createTileMap("levels/bars.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;
	player->setBars(map);
	map = TileMap::createTileMap("levels/escena1.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram,0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
	bplayer = true;
}

void Scene::change_scene2() {
	TileMap* map;
	map = TileMap::createTileMap("levels/scena2.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
}

void Scene::change_scene3() {
	TileMap* map;
	map = TileMap::createTileMap("levels/scena3.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
}

void Scene::change_scene4() {
	TileMap* map;
	map = TileMap::createTileMap("levels/scena4.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
}

void Scene::change_scene5() {
	TileMap* map;
	map = TileMap::createTileMap("levels/scena2.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
}

void Scene::change_instructions() {
	TileMap* map;
	map = TileMap::createTileMap("levels/instructions.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;

}

void Scene::change_credits() {
	/*escena creditos*/
}

void Scene::game_over() {
	PlaySound(TEXT("sounds/gameover.wav"), NULL, SND_FILENAME | SND_ASYNC);
	TileMap* map;
	map = TileMap::createTileMap("levels/gameover.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(33 * map->getTileSize(), 29 * map->getTileSize()));
}



