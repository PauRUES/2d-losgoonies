#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Enemy.h"


enum EnemyAnims {
	MOVE_LEFT, MOVE_RIGHT
};

Enemy::Enemy()
{
}

void Enemy::init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, string type)
{
	texProgram = shaderProgram;
	attackAngle = 0;
	width_attack = 30;
	spritesheet.loadFromFile("images/enemy.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32, 32), glm::vec2(1 / 2.f, 1 / 4.f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(2);

	if (type == "Calaveras") {
		sprite->setAnimationSpeed(MOVE_LEFT, 8);
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.f));
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(1 / 2.f, 0.f));


		sprite->setAnimationSpeed(MOVE_RIGHT, 8);
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 1 / 4.f));
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1 / 2.f, 1 / 4.f));
	}
	else {
		sprite->setAnimationSpeed(MOVE_LEFT, 8);
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 3 / 4.f));
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(1 / 2.f, 3 / 4.f));

		sprite->setAnimationSpeed(MOVE_RIGHT, 8);
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 2 / 4.f));
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1 / 2.f, 2 / 4.f));
	}

	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posEnemy.x), float(tileMapDispl.y + posEnemy.y)));



}

void Enemy::update(int deltaTime)
{
	float width = 680;
	float height = 480;
	float middle_x = width / 2.f;
	float middle_y = height / 2.f;

	sprite->update(deltaTime);
	posEnemy.x = int(startX - width_attack * sin(3.14159f * attackAngle / 180.f));

	if (attackAngle == 90) {
		sprite->changeAnimation(MOVE_LEFT);
	}
	else if (attackAngle == 180) {
		sprite->changeAnimation(MOVE_LEFT);
		attackAngle = 0;
	}


	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posEnemy.x), float(tileMapDispl.y + posEnemy.y)));
}

void Enemy::render()
{
	sprite->render();
}
